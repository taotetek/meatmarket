var PORT = process.env.PORT || 1983;
var restify = require('restify');
 
var options = {
    serverName: 'My server',
    accept: [ 'application/json' ]
}
 
var server = restify.createServer(options);
server.use(restify.CORS());
server.use(restify.bodyParser({ mapParams: false }));
 
server.listen(PORT, '0.0.0.0');
console.log("listening "+PORT);
 
var db_conn = require('./db_conn');
var statsResource = require('./stats');
statsResource.setAndConnectClient(db_conn.client);
 
server.get('/stats', function(req, res) {
    var stats = new statsResource.Stats();
    stats.getAllStats(function(result){
        var allStats = result;
        if(allStats.length == 0) {
            res.send(200, []);
            return;
        }else res.send(200, result);
    });    
});
