function Stats(){};
exports.Stats = Stats;
 
exports.setAndConnectClient = function(_client){
    client = _client;  
    client.connect();
}
 
Stats.prototype.getAllStats = function(callback){
    var allStats = [];
    var query = client.query('SELECT * FROM simple_stats', function(err,result) {
        if (!err) {
            allStats = result.rows;
        }else allStats = []
        if(allStats.length == 0) {
            callback([]); 
        }else callback(allStats);
    });        
}
